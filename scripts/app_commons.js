﻿(function () {

    /*
    it delays a given function for 500ms
    and cancels a previous waiting request
    */
    function delayFn(f) {
        var timer = null;
        return function () {
            var context = this,
				args = arguments;
            clearTimeout(timer);
            timer = window.setTimeout(
				function () {
				    f.apply(context, args);
				},
				500
			);
        };
    }


    /*
    given a template and a key-value object, it replaces the strings wrapped in {{}} with values from the obj
    and it returns the rendered template
    */
    function renderTemplate(template, obj) {

        var keysToFind = [],
            rendered = template;

        var regex = new RegExp("{{(.*?)}}", "g");
        keysToFind = template.match(regex);

        keysToFind.forEach(function (key) {
            var k = key.replace("{{", "").replace("}}", "");
            var val = obj[k];

            if (val != null) {
                // != instead of !== because we want to avoid entering this block if undefined
                // boolean values to String
                val = (val.toString().toLowerCase() === "false") ? "false" : val;
                val = (val.toString().toLowerCase() === "true") ? "true" : val;
            } else {
                val = "";
            }

            rendered = rendered.replace(key, val);
        });

        return rendered;
    }

    /*
    given a property, a text and, a template, (which should be commonTemplate) 
    it builds the necessary object to call renderTemplate
    it returns the rendered template or in case property does not exist, an empty string
    */
    function renderProperty(property, text, template) {
        if (property) {
            var obj = {
                text: text,
                property: property
            };
            return renderTemplate(template, obj);
        }
        else {
            return "";
        }
    }

    var commons = {
        delayFn: delayFn,
        renderTemplate: renderTemplate,
        renderProperty: renderProperty,
    };

    // expose commons
    window.commons = commons;

}());