$(document).ready(function() {

    // 'global' variables
    var page = 1,                // needed for the api calls
        pageLimit = 10,          // should be in alignment with corresponding variable in requests.js
		shownMovies = pageLimit, // count of movies currenlty displayed
		totalMovies = 0,         // count of all movies to be displayed 
		searchMode = false,     
		query = "",              // for the search
		moviesObj = {};          // object that holds all movies currently shown in page
	    
	
	$("#searchText").val('');
	showMovies();
	
    //-------------------------------------------------------------------------
    // Movies functions

    /*
    displays all the movies in the theaters or found depending on searchMode
    */
	function showMovies() {
		if (searchMode){
		    api.getMoviesBySearch(page, query).then(
                renderMovies,
                renderMoviesError);
		} else {
		    api.getMoviesInTheaters(page).then(
                renderMovies,
                renderMoviesError);
		}
	}
	
	function renderMoviesError(error) {
	    $("#moviesLoader").hide();
	    $("#underSearchDiv").html("<i>Loading error</i>");
	    console.log(error);
	}

    /*
    gets all html templates and renders all movies' information 
    by calling getMovieHtml for every movie
    and appends them in #moviesContainer
    */
	function renderMovies(data) {
	    totalMovies = data.total;
	    // message under search
	    $("#underSearchDiv").html( searchMode ? ("Found " + totalMovies + " movies") : (totalMovies + " movies in theaters this week"));

	    if (totalMovies > 0) {

	        if (totalMovies < pageLimit) {
	            // all movies fit in the first page, hide spinner
	            $("#moviesLoader").hide();
	        }

	        $.when($.get("../pages/templates/default.html"),
                    $.get("../pages/templates/expanded.html"),
                    $.get("../pages/templates/commonTemplate.html")).then(
                function (defaultTemplate, expandedTemplate, commonTemplate) {

                    var templatesObj = {
                        defaultTemplate: defaultTemplate[0],
                        expandedTemplate: expandedTemplate[0],
                        commonTemplate: commonTemplate[0]
                    };

                    // rows to be added in movies container
                    var $rows = $(document.createDocumentFragment());

	                data.movies.forEach(function (movie) {
	                    //if movie not already in movieObj 
	                    if (!moviesObj.hasOwnProperty(movie.id)) {
	                        // add movie data in movieObj 
	                        cacheMovieData(movie);
	                    }
	                    // movie row
	                    var row = '<div id="' + movie.id + '" class="row movieClass">';
	                    row += getMovieHtml(movie, templatesObj) + '</div>';
	                    $rows.append(row);
	                });

	                $("#moviesContainer").append($rows);
                },
                function (error) {
                    console.log(error);
                    $("#moviesContainer").append("<i>Loading error.</i>");
                });
	    }
	    else {
	        // no movies, hide bottom spinner
	        $("#moviesLoader").hide();
	    }
	}

    //-------------------------------------------------------------------------
    // Movie functions
	
    /*
    cache all movie info which is displayed in moviesObj
    */
	function cacheMovieData(movie){
	    
	    // at first movie state is normal  
	    movie.expanded = false;

	    // show up to 3 actors in cast 
	    var cast = "";
	    movie.abridged_cast.some(function(el, index, array){
	        cast = cast.concat(el.name);
	        if (index < 2 && index < array.length - 1){
	            cast = cast.concat(", ");
	        }
	        else if (index === 2 && index < array.length - 1){
	            cast = cast.concat(", ...");
	            return true;
	        }
	    });
	    movie.cast = cast;

        // save it in 'global' moviesObj
	    moviesObj[movie.id] = movie;
	}

    /*
    depending on movie's property "expanded", uses the corresponding html template
    and returns the movie's html
    */
	function getMovieHtml(movie, templates) {
        // get movie from moviesObj
	    movie = moviesObj[movie.id];

	    // template to render movie
	    var movieTemplate;

	    //render cast
	    var castHtml = commons.renderProperty(movie.cast, "Cast: ", templates.commonTemplate);
	    //render synopsis
	    var synopsisHtml = commons.renderProperty(movie.synopsis, "", templates.commonTemplate);

	    var movieObj = {
	        id: movie.id,
	        thumbnail: movie.posters.thumbnail,
	        title: movie.title,
	        year: (movie.year ? movie.year + ' - ' : ''),
	        runtime: (movie.runtime ? movie.runtime + ' mins - ' : ''),
	        rating: (movie.ratings.audience_score >= 0 ? movie.ratings.audience_score + '/100' : ''),
	        cast: castHtml,
	        synopsis: synopsisHtml
	    };

	    // depending on movie.expanded the movie template is the default or the expanded
	    if (movie.expanded) {
            // in case movie is expanded 
	        // render extra movie info
	        movieObj.genre = commons.renderProperty(movie.movieInfo.genre, "", templates.commonTemplate);
	        movieObj.director = commons.renderProperty(movie.movieInfo.director, "Director: ", templates.commonTemplate);
	        movieObj.similarMovies = commons.renderProperty(movie.movieInfo.similarMovies, "Similar movies: ", templates.commonTemplate);
	        movieObj.reviews = movie.movieInfo.reviews;

	        movieTemplate = templates.expandedTemplate;
		}
	    else {
	        movieTemplate = templates.defaultTemplate;
		}

	    return commons.renderTemplate(movieTemplate, movieObj);;
	}

    /*
    this is called when a movie is clicked for the first time
    it sends a request to the api in order to get the extra movie info 
    and saves it in a new movie property "movieInfo"
    */
	function requestMovieInfo(movie) {
	    var defer = new $.Deferred();
	    // request movie info and keep it in "movieInfo" property
	    $.when(api.getMovieInfo(movie.id), api.getSimilarMovies(movie.id), api.getMovieReviews(movie.id)).then(
            function (infoData, similarsData, reviewsData) {

                //movie genres, director
                movie.movieInfo = infoData;
                // similar movies
                movie.movieInfo.similarMovies = getSimilarMoviesHtml(similarsData.movies);
                // reviews
                movie.movieInfo.reviews = getReviewsHtml(reviewsData.reviews);

                defer.resolve(movie);
            },
            function (error) {
                defer.reject(error);
            });
	    return defer.promise();
	}

	/*
	gets all html templates and renders movie's information
    */
	function renderMovie(movie) {
	    var defer = new $.Deferred();
	    $.when($.get("../pages/templates/default.html"), $.get("../pages/templates/expanded.html"), $.get("../pages/templates/commonTemplate.html")).then(
            function (defaultTemplate, expandedTemplate, commonTemplate) {

                var templatesObj = {
                    defaultTemplate: defaultTemplate[0],
                    expandedTemplate: expandedTemplate[0],
                    commonTemplate: commonTemplate[0]
                };
                defer.resolve(getMovieHtml(movie, templatesObj));
            },
            function (error) {
                defer.reject(error);
            });
	    return defer.promise();
	}

    /*
    this is called when a movie panel is clicked
    it checks if the movie is clicked for the first time and renders it
    */
	function getMovieInfo(id) {
	    // get movie from moviesObj by id
	    var movie = moviesObj[id];
	    // alter movie state (expanded - default)
	    movie.expanded = !movie.expanded;

	    // check if movie has already a property with its info
	    // if not, request info and save it for later access
	    if (movie.expanded && !movie.hasOwnProperty("movieInfo")) {
	        // first time movie's info requested, show spinner until info is returned
	        appendMovieSpinner(id);
	        return requestMovieInfo(movie).then(renderMovie);
	    }
	    else {
	        return renderMovie(movie);
	    }
	}
	
    /*
    this is called when a movie panel is clicked for the first time, because a request to the api is sent
    and it stays visible waiting for the response
    */
	function appendMovieSpinner(movieId) {
	    var movieSpinner = '<div id="' + movieId + '-spinner" class="col-md-4">' +
							    '<img src="../images/loading.gif" alt="Loading movie info..."> ' +
						    '</div>';
	    $('#' + movieId).append(movieSpinner);
	}

    /*
    returns the html structure for the given similarMovies list 
    */
	function getSimilarMoviesHtml(similarMovies) {
	    var similarHtml = "";
	    if (similarMovies) {
	        similarMovies.forEach(function (similarMovie) {
	            // links are not handled
	            similarHtml += '<a href="#">' + similarMovie.title + '</a> &nbsp;';
	        });
	    }
	    return similarHtml;
	}

    /*
    returns the html structure for the given reviews list 
    */
	function getReviewsHtml(reviews) {
	    var reviewsHtml = "";
	    if (reviews && reviews.length > 0) {
	        reviews.forEach(function (review) {
	            reviewsHtml += '<div class="row">' +
                                    '<div class="col-md-12 marginBottom">' +
                                        '<div>' + review.quote + '</div>' +
                                        '<div class="reviewClass">' + (review.publication ? review.publication + ' | ' : '') +
                                        (review.date ? review.date : '') +
                                        '</div>' +
                                    '</div>' +
                                '</div>';
	        });
	    }
	    else {
	        reviewsHtml = '<div class="row">' +
							    '<div class="col-md-12 marginBottom">No reviews</div>' +
						    '</div>';
	    }
	    return reviewsHtml;
	}

    //-------------------------------------------------------------------------
    // Events 

	// when scroll down, show movies of next page
	$(window).scroll(function(){
	    if ($(window).scrollTop() + $(window).height() >= $(document).height()) {

	        // shownMovies equals to the movies currently displayed 
            // while totalMovies equals to all movies to be displayed
		   if (shownMovies < totalMovies) {
				page++;
				shownMovies += pageLimit;
				showMovies();
			}
			else {
				// no more movies to show, hide spinner
				$("#moviesLoader").hide();
			}
		}
	});
	
	// search button is clicked
	$("#searchBtn").on("click", function (e) {
	    searchMovies(e);
	});
 
	// text is typed or erased in search textbox
	// delay request, wait 500ms before calling search function
	// delayFn also cancels previous waiting request when search text changes
	$("#searchText").on("input", commons.delayFn(function(e){
	    searchMovies(e);
	}));

    // when movie is clicked: expand or return to normal state
	$('#moviesContainer').on('click', '.movieClass', function (e) {
	    e.preventDefault();
	    var id = this.id;
	    getMovieInfo(id).then(
            function (movieHtml) {
                $("#" + id).html(movieHtml);
            },
            function (error) {
                // expand has been canceled
                // return to normal state and hide spinner
                moviesObj[id].expanded = false;
                $('#' + id + '-spinner').remove();
                console.log(error);
            }
        );
	});
	
    // when a link is clicked, prevent clicking of movie's panel 
	$('#moviesContainer').on('click', 'a', function (e) {
	    e.stopImmediatePropagation();
	    return false;
	});

    // this is called when search button is clicked or a search query is typed
	function searchMovies(e) {
	    e.preventDefault();
        // trim the search text
	    query = $("#searchText").val().trim();

	    if (!query) {

	        // in case search text is erased return to view movies in theaters
	        if (searchMode) {
	            searchMode = false;
	            resetView();
	            showMovies();
	            return;
	        }
	        // do not accept strings only with spaces
	        $("#searchText").val('');
	        return;
	    }
	    searchMode = true;
	    resetView();
	    showMovies();
	}

    // reset 'global' variables
	function resetView() {
	    moviesObj = {};
	    shownMovies = pageLimit;
	    page = 1;
	    // show spinner again in case it was hidden before
	    $("#moviesLoader").show();
	    $("#underSearchDiv").html("");
	    $("#moviesContainer").html("");
	}

    //----------------------------------------------------------------------

});