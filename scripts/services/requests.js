(function(){
	
	// Web API vars---------------------------------------------
    var apiEndpoint = "http://api.rottentomatoes.com",
		apiKey = "qtqep7qydngcc7grk4r4hyd9",    // not valid anymore
        pageLimit = 10,     // pagination: 10 movies in each page
        country = "us",     
		similarsLimit = 2,  // request only 2 similar movies
		reviewType = "all", 
		reviewsLimit = 2,   // request only the 2 latest movie reviews
		reviewsPage = 1;    // so just one page
	
	// common ajax call for all requests
    function getData(endpoint) {
        return $.ajax({
            type: "GET",
			dataType: "jsonp",
            async: true,
            url: endpoint,
            contentType: "application/json",
        });
    }
	
	function getMoviesInTheaters(page) {
		var endpoint = apiEndpoint + "/api/public/v1.0/lists/movies/in_theaters.json?page_limit=" + pageLimit + "&page=" + page + "&country=" + country + "&apikey=" + apiKey;
		return getData(endpoint);

	    // for mock data leave only the next lines uncommented
		//var defer = new $.Deferred();
		//defer.resolve(moviesData[page]);
		//return defer.promise();
	}
	
	function getMoviesBySearch(page, query){
		var endpoint = apiEndpoint + "/api/public/v1.0/movies.json?q=" + query + "&page_limit=" + pageLimit + "&page=" + page + "&apikey="+ apiKey;
		return getData(endpoint);

	    // for mock data leave only the next lines uncommented
		//var defer = new $.Deferred();
		//defer.resolve(moviesData[page]);
		//return defer.promise();
	}
	
	function getMovieInfo(id){
		var endpoint = apiEndpoint + "/api/public/v1.0/movies/" + id + ".json?" + "&apikey="+ apiKey;
		return getData(endpoint)
            .then(function(data){
				// return just the wanted info: movie genres and one director
				var movieGenre = data.genres ? data.genres.join(" - ") : "";
				var movieDirector = (data.abridged_directors && data.abridged_directors.length > 0) ? data.abridged_directors[0].name : "";
				var info = {
					genre: movieGenre,
					director: movieDirector
				};
				return info;
            });

	    // for mock data leave only the next line uncommented
	    //return movieInfo;
	}
	
	function getSimilarMovies(id) {
		// get up to 2 similar movies
		var endpoint = apiEndpoint + "/api/public/v1.0/movies/" + id + "/similar.json?limit=" + similarsLimit + "&apikey=" + apiKey;
		return getData(endpoint);

	    // for mock data leave only the next line uncommented
	    //return similarMovies;
	}
	
	function getMovieReviews(id){
		// get up to 2 reviews
		var endpoint = apiEndpoint + "/api/public/v1.0/movies/" + id + "/reviews.json?review_type=" + reviewType + "&page_limit=" + reviewsLimit + "&page=" + reviewsPage + "&country=" + country + "&apikey="+ apiKey;
		return getData(endpoint);

	    // for mock data leave only the next line uncommented
	    //return movieReviews;
	}
	
	var api = {
		getMoviesInTheaters: getMoviesInTheaters,
		getMoviesBySearch: getMoviesBySearch,
		getMovieInfo: getMovieInfo,
		getSimilarMovies: getSimilarMovies,
		getMovieReviews: getMovieReviews
	};
	
	// expose api
	window.api = api;


    //---------------------------------------------------------
    // mock data

    /*
    Rotten Tomatoes (RT) JSON API cannot be used beacause of the invalid api key
    So, mock data can be used to check the page's functionality.
    Search won't be working properly but the events of typing and search button clicking can be tested.
    There are 21 movies in 3 pages.
    */

	var movieInfo = {
	    "genre": "Animation - Comedy",
	    "director": "Lee Unkrich"
	};

	var similarMovies = {
	    "movies": [
            { "title": "Frozen" },
            { "title": "Finding Nemo" }
	    ]
	};

	var movieReviews = {
	    "reviews": [
            {
                "quote": "Amazing",
                "publication": "New Yorker",
                "date": "2015-09-03"
            },
            {
                "quote": "Deftly blending comedy, adventure, and honest emotion",
                "publication": "Mary Rickles",
                "date": "2014-04-08"
            }
	    ]
	};

	var moviesData = {
	    "1": {
	        "total": 21,
	        "movies": [
                {
                    "id": 770672110,
                    "title": "Toy Story 1",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                {
                    "id": 770672111,
                    "title": "Toy Story 2",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                {
                    "id": 770672112,
                    "title": "Toy Story 3",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                 {
                     "id": 770672113,
                     "title": "Toy Story 4",
                     "year": 2010,
                     "genres": [
                       "Animation",
                       "Comedy",
                       "Kids & Family",
                       "Science Fiction & Fantasy"
                     ],
                     "mpaa_rating": "G",
                     "runtime": 103,
                     "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                     "release_dates": {
                         "theater": "2010-06-18",
                         "dvd": "2010-11-02"
                     },
                     "ratings": {
                         "critics_rating": "Certified Fresh",
                         "critics_score": 99,
                         "audience_rating": "Upright",
                         "audience_score": 91
                     },
                     "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                     "posters": {
                         "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                     },
                     "abridged_cast": [
                       {
                           "name": "Tom Hanks",
                           "characters": ["Woody"]
                       },
                       {
                           "name": "Tim Allen",
                           "characters": ["Buzz Lightyear"]
                       },
                       {
                           "name": "Joan Cusack",
                           "characters": ["Jessie the Cowgirl"]
                       },
                       {
                           "name": "Don Rickles",
                           "characters": ["Mr. Potato Head"]
                       },
                       {
                           "name": "Wallace Shawn",
                           "characters": ["Rex"]
                       }
                     ],
                     "abridged_directors": [{ "name": "Lee Unkrich" }],
                     "studio": "Walt Disney Pictures",
                     "links": {
                         "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                         "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                         "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                         "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                         "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                         "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                     }
                 },
                {
                    "id": 770672114,
                    "title": "Toy Story 5",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                {
                    "id": 770672116,
                    "title": "Toy Story 6",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                 {
                     "id": 770672117,
                     "title": "Toy Story 7",
                     "year": 2010,
                     "genres": [
                       "Animation",
                       "Comedy",
                       "Kids & Family",
                       "Science Fiction & Fantasy"
                     ],
                     "mpaa_rating": "G",
                     "runtime": 103,
                     "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                     "release_dates": {
                         "theater": "2010-06-18",
                         "dvd": "2010-11-02"
                     },
                     "ratings": {
                         "critics_rating": "Certified Fresh",
                         "critics_score": 99,
                         "audience_rating": "Upright",
                         "audience_score": 91
                     },
                     "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                     "posters": {
                         "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                     },
                     "abridged_cast": [
                       {
                           "name": "Tom Hanks",
                           "characters": ["Woody"]
                       },
                       {
                           "name": "Tim Allen",
                           "characters": ["Buzz Lightyear"]
                       },
                       {
                           "name": "Joan Cusack",
                           "characters": ["Jessie the Cowgirl"]
                       },
                       {
                           "name": "Don Rickles",
                           "characters": ["Mr. Potato Head"]
                       },
                       {
                           "name": "Wallace Shawn",
                           "characters": ["Rex"]
                       }
                     ],
                     "abridged_directors": [{ "name": "Lee Unkrich" }],
                     "studio": "Walt Disney Pictures",
                     "links": {
                         "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                         "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                         "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                         "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                         "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                         "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                     }
                 },
                {
                    "id": 770672118,
                    "title": "Toy Story 8",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                {
                    "id": 770672119,
                    "title": "Toy Story 9",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                 {
                     "id": 770672120,
                     "title": "Toy Story 10",
                     "year": 2010,
                     "genres": [
                       "Animation",
                       "Comedy",
                       "Kids & Family",
                       "Science Fiction & Fantasy"
                     ],
                     "mpaa_rating": "G",
                     "runtime": 103,
                     "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                     "release_dates": {
                         "theater": "2010-06-18",
                         "dvd": "2010-11-02"
                     },
                     "ratings": {
                         "critics_rating": "Certified Fresh",
                         "critics_score": 99,
                         "audience_rating": "Upright",
                         "audience_score": 91
                     },
                     "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                     "posters": {
                         "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                     },
                     "abridged_cast": [
                       {
                           "name": "Tom Hanks",
                           "characters": ["Woody"]
                       },
                       {
                           "name": "Tim Allen",
                           "characters": ["Buzz Lightyear"]
                       },
                       {
                           "name": "Joan Cusack",
                           "characters": ["Jessie the Cowgirl"]
                       },
                       {
                           "name": "Don Rickles",
                           "characters": ["Mr. Potato Head"]
                       },
                       {
                           "name": "Wallace Shawn",
                           "characters": ["Rex"]
                       }
                     ],
                     "abridged_directors": [{ "name": "Lee Unkrich" }],
                     "studio": "Walt Disney Pictures",
                     "links": {
                         "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                         "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                         "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                         "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                         "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                         "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                     }
                 }
	        ]
	    },
	    "2": {
	        "total": 21,
	        "movies": [
                {
                    "id": 270672110,
                    "title": "Toy Story 11",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                {
                    "id": 270672111,
                    "title": "Toy Story 12",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                {
                    "id": 270672112,
                    "title": "Toy Story 13",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                 {
                     "id": 270672113,
                     "title": "Toy Story 14",
                     "year": 2010,
                     "genres": [
                       "Animation",
                       "Comedy",
                       "Kids & Family",
                       "Science Fiction & Fantasy"
                     ],
                     "mpaa_rating": "G",
                     "runtime": 103,
                     "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                     "release_dates": {
                         "theater": "2010-06-18",
                         "dvd": "2010-11-02"
                     },
                     "ratings": {
                         "critics_rating": "Certified Fresh",
                         "critics_score": 99,
                         "audience_rating": "Upright",
                         "audience_score": 91
                     },
                     "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                     "posters": {
                         "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                     },
                     "abridged_cast": [
                       {
                           "name": "Tom Hanks",
                           "characters": ["Woody"]
                       },
                       {
                           "name": "Tim Allen",
                           "characters": ["Buzz Lightyear"]
                       },
                       {
                           "name": "Joan Cusack",
                           "characters": ["Jessie the Cowgirl"]
                       },
                       {
                           "name": "Don Rickles",
                           "characters": ["Mr. Potato Head"]
                       },
                       {
                           "name": "Wallace Shawn",
                           "characters": ["Rex"]
                       }
                     ],
                     "abridged_directors": [{ "name": "Lee Unkrich" }],
                     "studio": "Walt Disney Pictures",
                     "links": {
                         "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                         "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                         "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                         "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                         "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                         "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                     }
                 },
                {
                    "id": 270672114,
                    "title": "Toy Story 15",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                {
                    "id": 270672116,
                    "title": "Toy Story 16",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                 {
                     "id": 270672117,
                     "title": "Toy Story 17",
                     "year": 2010,
                     "genres": [
                       "Animation",
                       "Comedy",
                       "Kids & Family",
                       "Science Fiction & Fantasy"
                     ],
                     "mpaa_rating": "G",
                     "runtime": 103,
                     "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                     "release_dates": {
                         "theater": "2010-06-18",
                         "dvd": "2010-11-02"
                     },
                     "ratings": {
                         "critics_rating": "Certified Fresh",
                         "critics_score": 99,
                         "audience_rating": "Upright",
                         "audience_score": 91
                     },
                     "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                     "posters": {
                         "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                     },
                     "abridged_cast": [
                       {
                           "name": "Tom Hanks",
                           "characters": ["Woody"]
                       },
                       {
                           "name": "Tim Allen",
                           "characters": ["Buzz Lightyear"]
                       },
                       {
                           "name": "Joan Cusack",
                           "characters": ["Jessie the Cowgirl"]
                       },
                       {
                           "name": "Don Rickles",
                           "characters": ["Mr. Potato Head"]
                       },
                       {
                           "name": "Wallace Shawn",
                           "characters": ["Rex"]
                       }
                     ],
                     "abridged_directors": [{ "name": "Lee Unkrich" }],
                     "studio": "Walt Disney Pictures",
                     "links": {
                         "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                         "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                         "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                         "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                         "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                         "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                     }
                 },
                {
                    "id": 270672118,
                    "title": "Toy Story 18",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                {
                    "id": 270672119,
                    "title": "Toy Story 19",
                    "year": 2010,
                    "genres": [
                      "Animation",
                      "Comedy",
                      "Kids & Family",
                      "Science Fiction & Fantasy"
                    ],
                    "mpaa_rating": "G",
                    "runtime": 103,
                    "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                    "release_dates": {
                        "theater": "2010-06-18",
                        "dvd": "2010-11-02"
                    },
                    "ratings": {
                        "critics_rating": "Certified Fresh",
                        "critics_score": 99,
                        "audience_rating": "Upright",
                        "audience_score": 91
                    },
                    "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                    "posters": {
                        "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                        "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                    },
                    "abridged_cast": [
                      {
                          "name": "Tom Hanks",
                          "characters": ["Woody"]
                      },
                      {
                          "name": "Tim Allen",
                          "characters": ["Buzz Lightyear"]
                      },
                      {
                          "name": "Joan Cusack",
                          "characters": ["Jessie the Cowgirl"]
                      },
                      {
                          "name": "Don Rickles",
                          "characters": ["Mr. Potato Head"]
                      },
                      {
                          "name": "Wallace Shawn",
                          "characters": ["Rex"]
                      }
                    ],
                    "abridged_directors": [{ "name": "Lee Unkrich" }],
                    "studio": "Walt Disney Pictures",
                    "links": {
                        "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                        "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                        "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                        "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                        "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                        "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                    }
                },
                 {
                     "id": 270672120,
                     "title": "Toy Story 20",
                     "year": 2010,
                     "genres": [
                       "Animation",
                       "Comedy",
                       "Kids & Family",
                       "Science Fiction & Fantasy"
                     ],
                     "mpaa_rating": "G",
                     "runtime": 103,
                     "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                     "release_dates": {
                         "theater": "2010-06-18",
                         "dvd": "2010-11-02"
                     },
                     "ratings": {
                         "critics_rating": "Certified Fresh",
                         "critics_score": 99,
                         "audience_rating": "Upright",
                         "audience_score": 91
                     },
                     "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                     "posters": {
                         "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                     },
                     "abridged_cast": [
                       {
                           "name": "Tom Hanks",
                           "characters": ["Woody"]
                       },
                       {
                           "name": "Tim Allen",
                           "characters": ["Buzz Lightyear"]
                       },
                       {
                           "name": "Joan Cusack",
                           "characters": ["Jessie the Cowgirl"]
                       },
                       {
                           "name": "Don Rickles",
                           "characters": ["Mr. Potato Head"]
                       },
                       {
                           "name": "Wallace Shawn",
                           "characters": ["Rex"]
                       }
                     ],
                     "abridged_directors": [{ "name": "Lee Unkrich" }],
                     "studio": "Walt Disney Pictures",
                     "links": {
                         "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                         "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                         "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                         "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                         "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                         "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                     }
                 }
	        ]
	    },
	    "3": {
	        "total": 21,
	        "movies": [
                 {
                     "id": 370672131,
                     "title": "Toy Story 21",
                     "year": 2010,
                     "genres": [
                       "Animation",
                       "Comedy",
                       "Kids & Family",
                       "Science Fiction & Fantasy"
                     ],
                     "mpaa_rating": "G",
                     "runtime": 103,
                     "critics_consensus": "Deftly blending comedy, adventure, and honest emotion, Toy Story 3 is a rare second sequel that really works.",
                     "release_dates": {
                         "theater": "2010-06-18",
                         "dvd": "2010-11-02"
                     },
                     "ratings": {
                         "critics_rating": "Certified Fresh",
                         "critics_score": 99,
                         "audience_rating": "Upright",
                         "audience_score": 91
                     },
                     "synopsis": "Pixar returns to their first success with Toy Story 3. The movie begins with Andy leaving for college and donating his beloved toys -- including Woody (Tom Hanks) and Buzz (Tim Allen) -- to a daycare. While the crew meets new friends, including Ken (Michael Keaton), they soon grow to hate their new surroundings and plan an escape. The film was directed by Lee Unkrich from a script co-authored by Little Miss Sunshine scribe Michael Arndt. ~ Perry Seibert, Rovi",
                     "posters": {
                         "thumbnail": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "profile": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "detailed": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg",
                         "original": "http://content6.flixster.com/movie/11/13/43/11134356_tmb.jpg"
                     },
                     "abridged_cast": [
                       {
                           "name": "Tom Hanks",
                           "characters": ["Woody"]
                       },
                       {
                           "name": "Tim Allen",
                           "characters": ["Buzz Lightyear"]
                       },
                       {
                           "name": "Joan Cusack",
                           "characters": ["Jessie the Cowgirl"]
                       },
                       {
                           "name": "Don Rickles",
                           "characters": ["Mr. Potato Head"]
                       },
                       {
                           "name": "Wallace Shawn",
                           "characters": ["Rex"]
                       }
                     ],
                     "abridged_directors": [{ "name": "Lee Unkrich" }],
                     "studio": "Walt Disney Pictures",
                     "links": {
                         "self": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122.json",
                         "alternate": "http://www.rottentomatoes.com/m/toy_story_3/",
                         "cast": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/cast.json",
                         "clips": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/clips.json",
                         "reviews": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json",
                         "similar": "http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/similar.json"
                     }
                 }
	        ]
	    }
	    
	};

}());