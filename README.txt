I. Application Description
--------------------------
The MovieRama application is yet another movie catalog 
where users can check the movies of the week in theaters, search for movies and view details about them. 
It is a client�side only � single page application, which solely relies on Rotten Tomatoes (RT) JSON API for a data source.

MovieRama uses 'infinite scrolling'. 
It starts with a list of 10 movies and each time users scroll near the bottom of the page another 10 are loaded and added to the list.

Functionality:
Users are able to browse through the movies of the week using infinite scrolling.
Users are able to search for movies by typing into a search textbox and then browse through the results using infinite scrolling.
Users are able to click on a movie to view more info about it. 
Clicking on it expands the movie panel to fit the extra information while clicking on it again returns it to its normal state.


II. File list
-------------
pages/index.html			- main application page
pages/templates				- folder that contains all html templates to render movie information
content/main.css			- main custom css file
content/bootstrap.min.css
content/bootstrap-theme.min.css		- files that contain bootstrap css framework
images/favicon.gif			- application page icon
images/loading.gif			- spinner image
scripts/app.js				- main application script file
scripts/app_commons.js			- file that contains helper functions
scripts/services/requests.js		- api requests
scripts/lib/jquery-1.12.0.min.js	- file that contains jquery library
README.txt				- this file